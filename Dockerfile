FROM alpine:latest
LABEL maintainer="Thomas Citharel <tcit@tcit.fr>"
ENV MKDOCS_VERSION=1.5.2 \
    REFRESHED_AT=2023-08-30


RUN \
    apk add --update \
    ca-certificates \
    bash \
    git \
    openssh \
    python3 \
    python3-dev \
    py3-pip \
    build-base && \
    pip install --upgrade pip && \
    pip install mkdocs==${MKDOCS_VERSION} mkdocs[i18n] mkdocs-material pymdown-extensions pygments mkdocs-git-revision-date-localized-plugin mkdocs-minify-plugin mkdocs-exclude && \
    rm -rf /tmp/* /var/tmp/* /var/cache/apk/* /var/cache/distfiles/*

