# Maintenance tasks

## Installation

Generates new configuration files.

!!! warning
    This command generates configuration for your Mobilizon instance and should be run only once when installing. 

If any options are left unspecified, you will be prompted interactively.

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl instance gen [<options>]
    ```

=== "Docker"
    You shouldn't need to run this task on Docker.

=== "Source"
    ```bash
    mix mobilizon.instance gen [<options>]
    ```
### Options
* `-f`, `--force` Whether to erase existing files
* `-o`, `--output PATH` The path to output the config file. Defaults to `config/runtime.exs`.
* `--output-psql PATH` The path to output the SQL script. Defaults to `setup_db.psql`.
* `--domain DOMAIN` The instance's domain
* `--instance-name INSTANCE_NAME` The instance's name
* `--admin-email ADMIN_EMAIL` The administrator's email
* `--dbhost HOSTNAME` The database hostname of the PostgreSQL database to use
* `--dbname DBNAME` The name of the database to use 
* `--dbuser DBUSER` The database user (aka role) to use for the database connection 
* `--dbpass DBPASS` The database user's password to use for the database connection 
* `--dbport DBPORT` The database port
* `--listen-port PORT` The port the app should listen to, defaults to 4000

## Detect spam in existing content

*Added in Mobilizon 3.1.0*

Detect spam based on user and profile data (only profile if remote) as well as events.

=== "Release"
    ```bash
    sudo -u mobilizon ./bin/mobilizon_ctl maintenance.detectspam [<options>]
    ```

=== "Docker"
    ```bash
    docker compose exec mobilizon mobilizon_ctl maintenance.detectspam [<options>]
    ```

=== "Source"
    ```bash
    mix mobilizon.maintenance.detect_spam [<options>]
    ```
### Options
* `-d`, `--dry_run` Only list detected spam, don't create reports
* `-v`, `--verbose` Verbose output
* `-l`, `--local_only` Only look at local content. Recommended.
* `-f`, `--forward_reports` Forward reports from remote content to remote instances. Use with caution, as the spam detection system can report a lot of false positives.
