---
title: Release
---

# Preparing

The release page contains a changelog, and below it, upgrade instructions. **Read and understand** the release instructions.

Some tasks (like database migrations) can take a while, so we advise you to run commands inside a `tmux` or `screen`.

# Backup

Always make sure your database, config file (by default at `/etc/mobilizon/config.exs`) and media uploads directory are properly backuped before performing upgrades.

Unless stated otherwise in the release notes, the following steps are enough to upgrade Mobilizon.

# Steps

### Fetch latest build

!!! info
    To get the latest package URL, you should go to [our release page](https://framagit.org/framasoft/mobilizon/-/releases) and copy the URL from the latest `tar.gz` package.
    
    The https://joinmobilizon.org/latest-package URL redirects to [the latest <ins>stable</ins> release package available](https://joinmobilizon.org/latest-release) ([Gitlab doesn't allow to do this directly currently](https://gitlab.com/gitlab-org/gitlab/-/issues/16821)), but you shouldn't use it it you want to get a beta version or a different version.

!!! info "Architecture"
    The build behind `https://joinmobilizon.org/latest-package` targets architecture `amd64`. If you need `arm` or `arm64`, you'll find links to builds for these architectures [on the release page](https://joinmobilizon.org/latest-release).

* Download the latest release build and extract it:
  ```sh
  curl -L 'https://joinmobilizon.org/latest-package' -o /tmp/mobilizon.tar.gz
  tar xzf /tmp/mobilizon.tar.gz -C /tmp/
  ```

### Perform the upgrade
* Stop the running Mobilizon process
  ```sh
  sudo systemctl stop mobilizon
  ```

* Move the release to the install folder, replacing the previous install:  
  ```sh
  sudo rm -rf /opt/mobilizon
  sudo mv /tmp/mobilizon /opt/
  sudo rm /tmp/mobilizon.tar.gz
  ```

* Set the proper owner to `/opt/mobilizon`
  ```sh
  sudo chown -R mobilizon:mobilizon /opt/mobilizon
  ```

### Perform the eventual database migrations

You can run this command safely even if no migrations are needed.

```bash
cd /opt/mobilizon
sudo -u mobilizon ./bin/mobilizon_ctl migrate
```
### Restart Mobilizon
Let's switch back one last time to your regular user.
```bash
sudo systemctl restart mobilizon
```

You can follow the Mobilizon logs to check that everything works properly. 
```bash
sudo journalctl -u mobilizon -f
```

**That’s all!** You’re running the new version of Mobilizon now.

If you have issues after upgrading, try reviewing upgrade steps and release notes.
Then feel free to [contact us](../../about.md#discuss) or file an issue on [our Gitlab](https://framagit.org/framasoft/mobilizon/issues)
