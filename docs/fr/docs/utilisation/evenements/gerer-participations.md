# Gérer les participations à un événement

## Liste des participant⋅e⋅s

En tant que profil ayant créé l'événement vous pouvez voir qui participe (ou demande à participer) à votre événement. Pour cela vous devez&nbsp;:

  * cliquer sur **X personnes y vont**&nbsp;:

    ![nombre de participant⋅e⋅s](../../images/event-participations-number-event-page-FR.png)

  * ou sur **Mes événements** dans le menu supérieur puis sur **Gérer les particpations**&nbsp;:

    ![actions événements](../../images/events-page-manage-participations-FR.png)

![événements participations liste](../../images/event-participations-list-messages-FR.png)

## Refuser une participation

![gif pour refuser une participation](../../images/event-participations-list-messages-reject-FR.gif)

Pour refuser une participation vous devez [aller dans la liste](#liste-des-participantes) et&nbsp;:

  1. cocher la case devant la participation à refuser
  * cliquer sur le bouton **Refuser le ou la participant⋅e**

Une fois fait, la participation sera marquée comme rejetée dans la liste&nbsp;:

![participation rejetée label](../../images/event-participations-list-messages-rejected-FR .png)

!!! info
    La personne sera notifiée par mail du refus.

## Approuver une participation

Lorsque l'option **Je veux approuver chaque demande de participation** est activée pour l'événement, une fenêtre de confirmation sera affichée avec la possibilité d'ajouter un petit texte motivant votre demande&nbsp;:

![modale de confirmation de participation](../../images/event-participation-confirmation-FR.png)

Les organisateurs et organisatrices peuvent approuver les participations en allant dans [la liste](#liste-des-participantes) et&nbsp;:

  1. en cochant la case devant la ou les participation(s)
  * en cliquant sur le bouton **Valider le ou la participant⋅e**

![liste participations](../../images/event-participation-approval-FR.png)

Une fois fait, la participation sera maqué comme acceptée dans la liste&nbsp;:

![participation acceptée](../../images/event-participation-approved-FR.png)
