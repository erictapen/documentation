# Participer à un événement

## Avec un compte

Quand vous souhaitez participer à un événement, vous devez&nbsp;:

  1. aller sur la page de l'événement (c'est quand plus pratique ;))
  * cliquer sur le bouton **Participer** sous l'image d'illustration
  * sélectionner le profil avec lequel vous souhaitez y participer

![image de participation avec compte](../../images/event-tree-participation-rose-utopia-FR.jpg)

C'est tout&nbsp;! Le bouton **Participer** devient **Je participe**.

!!! note
    Si vous cliquer sur le bouton **Participer** sans être connecté⋅e à votre compte, vous serez invité⋅e à le faire.

!!! info
    Si les participations doivent être approuvées, vous devez attendre qu'un⋅e organisateur⋅rice approuve votre demande.

## Anonymement

!!! note
    Les organisateurs/organisatrices doivent **autoriser** [les participations anonymes](./creation-evenement.md/#qui-peut-voir-cet-evenement-et-y-participer). Par défaut, elles ne le sont pas.


Pour participer anonymement, vous devez&nbsp;:

  1. aller sur la page de l'événement (c'est quand même plus pratique ;))
  * cliquer sur le bouton **Participer** sous l'image d'illustration
  * cliquer sur le bouton **Je n'ai pas de compte Mobilizon** pour participer en utilisant une adresse mail
  * entrer votre adresse mail dans le champ **Adresse email**
  * [Optionnel] écrire un message à l'attention des organisateurs et organisatrices
  * cliquer sur le bouton **Envoyer un email**

![demande web participation anonyme](../../images/rose-utopia-anonymous-participation-web-FR.png)

un email de confirmation vous sera envoyé pour confirmer votre participation. Cliquez sur le bouton **Confirmer mon addresse email**. Félicitations&nbsp: votre participation a été validée.

### Se souvenir de ma participation dans ce navigateur

**Optionnel**&nbsp;: vous pouvez cocher la case **Se souvenir de ma participation dans ce navigateur**. Cela permettra d'afficher et de gérer votre statut de participation sur la page de l'événement lorsque vous utilisez ce navigateur. Décochez si vous utilisez un appareil public.

Si la case est cochée, vous pouvez annuler votre participation anonyme en cliquant sur le bouton **Annuler la participation anonyme** situé sous la bannière de l'événement.

!!! info
    Si les participations doivent être approuvées, vous devez attendre qu'un⋅e organisateur⋅rice approuve votre demande.
